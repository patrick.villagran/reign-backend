import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ArticlesModule } from './modules/articles/articles.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('/api/v1');
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );
  const options = new DocumentBuilder()
    .setTitle('Articles')
    .setDescription('List of articles from hackers news')
    .setVersion('1.0')
    .addTag('articles')
    .addBearerAuth({
      type: 'http',
      scheme: 'bearer',
      bearerFormat: 'JWT',
      name: 'JWT',
      description: 'Enter JWT token',
      in: 'header',
    })
    .build();

  const articleDocument = SwaggerModule.createDocument(app, options, {
    include: [ArticlesModule],
  });

  SwaggerModule.setup('api/articles', app, articleDocument);

  await app.listen(3000);
}
bootstrap();
