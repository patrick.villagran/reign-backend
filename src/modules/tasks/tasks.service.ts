import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { HttpService } from '@nestjs/axios';

import { ArticlesService } from '../articles/articles.service';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(
    private articlesService: ArticlesService,
    private http: HttpService,
  ) {
    this.insertLastArticles('Starting the first data insertion');
  }

  @Cron(CronExpression.EVERY_HOUR)
  async insertLastArticles(msg = 'Initiating verification every hour') {
    this.logger.log(msg);
    const data = await this.http.get(process.env.HACKERS_API_URI).toPromise();
    await this.articlesService.createArticleBulk(data.data.hits);
  }
}
