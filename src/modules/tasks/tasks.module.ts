import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';

import { ArticlesModule } from '../articles/articles.module';
import { TasksService } from './tasks.service';

@Module({
  providers: [TasksService],
  imports: [ArticlesModule, HttpModule],
})
export class TasksModule {}
