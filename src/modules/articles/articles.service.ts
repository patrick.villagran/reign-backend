import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { NotFoundException } from '@nestjs/common';

import { Article, ArticleDocument } from './schemas/article.schema';
import { ArticlePaginationParams, CreateArticleDTO } from './dto/article.dto';
import { DateUtils } from '../../utils/DateUtils';

@Injectable()
export class ArticlesService {
  private readonly logger = new Logger(ArticlesService.name);

  constructor(
    @InjectModel(Article.name) private articleModel: Model<ArticleDocument>,
  ) {}

  async getArticles(filters: ArticlePaginationParams): Promise<unknown> {
    let options = {};

    options = {
      $or: [
        {
          title: { $regex: filters.title, $options: 'i' },
          author: { $regex: filters.author, $options: 'i' },
          tags: filters.tags.length
            ? { $all: filters.tags }
            : { $exists: true },
          $expr: filters.month
            ? {
                $eq: [
                  { $month: '$created_at' },
                  DateUtils.getMonthByName(filters.month),
                ],
              }
            : {},
        },
      ],
    };
    const query = this.articleModel.find(options).sort({ _id: 1 });
    const limit = filters.limit;
    const total = await this.articleModel.count(options);

    const data: Article[] = await query
      .skip((filters.page - 1) * filters.limit)
      .limit(filters.limit)
      .exec();

    return {
      data,
      total,
      page: filters.page,
      last_page: Math.ceil(total / limit),
    };
  }

  async createArticle(CreateArticleDTO: CreateArticleDTO): Promise<Article> {
    const newArticle = new this.articleModel(CreateArticleDTO);
    return await newArticle.save();
  }

  async createArticleBulk(articles: Array<any>): Promise<void> {
    const models = [];

    for (const item of articles) {
      const title = item.story_title ? item.story_title : item.title;
      if (title) {
        const model = new this.articleModel({
          author: item.author,
          title,
          tags: item._tags,
          created_at: item.created_at,
        });

        const isDuplicate = await this.articleModel.exists({
          title,
          created_at: item.created_at,
        });

        if (!isDuplicate) models.push(model);
      }
    }

    const inserts = await this.articleModel.bulkSave(models);

    if (inserts.result) {
      this.logger.log('OK Insert data from External Api');
    } else {
      this.logger.error('ERROR Insert data from External Api');
    }
  }

  async removeArticleById(articleId: string): Promise<void> {
    const result = await this.articleModel.findByIdAndDelete(articleId);
    if (!result) {
      throw new NotFoundException();
    }
  }
}
