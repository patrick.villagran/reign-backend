import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional, IsString } from 'class-validator';
import { PaginationParams } from '../../../utils/PaginationParams';

export class CreateArticleDTO {
  readonly author: string;
  readonly title: string;
  readonly tags: Array<string>;
  readonly created_at: Date;
}

export class ArticleDTO {
  @ApiProperty()
  @ApiPropertyOptional()
  readonly author: string;

  @ApiProperty()
  @ApiPropertyOptional()
  readonly title: string;

  @ApiProperty()
  @ApiPropertyOptional()
  readonly tags: Array<string>;

  readonly created_at: Date;
}

export class ArticlePaginationParams extends PaginationParams {
  @IsOptional()
  @Type(() => String)
  @IsString()
  @ApiProperty()
  @ApiPropertyOptional()
  month?: string = '';

  @IsOptional()
  @Type(() => String)
  @IsString()
  @ApiProperty()
  @ApiPropertyOptional()
  readonly author?: string = '';

  @IsOptional()
  @Type(() => String)
  @IsString()
  @ApiProperty()
  @ApiPropertyOptional()
  readonly title: string = '';

  @IsOptional()
  @ApiProperty()
  @ApiPropertyOptional()
  readonly tags: string[] = [];
}
