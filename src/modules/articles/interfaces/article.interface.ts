import { Document } from 'mongoose';

export interface Article extends Document {
  readonly author: string;
  readonly title: string;
  readonly tags: Array<string>;
  readonly created_at: Date;
}
