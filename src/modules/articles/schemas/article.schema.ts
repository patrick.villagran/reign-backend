import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop()
  @ApiProperty()
  author: string;

  @Prop()
  @ApiProperty()
  title: string;

  @Prop([String])
  @ApiProperty()
  tags: string[];

  @Prop()
  created_at: Date;
}

const ArticleSchema = SchemaFactory.createForClass(Article);

ArticleSchema.virtual('month').get(function (this: ArticleDocument) {
  return this.created_at
    .toLocaleString('default', { month: 'long' })
    .toLowerCase();
});

export { ArticleSchema };
