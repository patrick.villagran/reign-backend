import { PartialType } from '@nestjs/swagger';
import { Article } from '../schemas/article.schema';

export class IndexArticleSwagger extends PartialType(Article) {}
