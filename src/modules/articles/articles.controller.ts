import {
  Controller,
  Delete,
  Get,
  Param,
  Query,
  Post,
  Body,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse } from '@nestjs/swagger';

import { ArticlePaginationParams, CreateArticleDTO } from './dto/article.dto';
import { ArticlesService } from './articles.service';
import { IndexArticleSwagger } from './swagger/index-article.swagger';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';

@Controller('articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  @ApiOperation({ summary: 'List all articles with parameters' })
  @ApiResponse({
    status: 200,
    description: 'List of articles return with success',
    type: IndexArticleSwagger,
    isArray: true,
  })
  @ApiBearerAuth()
  getArticles(@Query() filters: ArticlePaginationParams): Promise<unknown> {
    console.log(filters);
    return this.articlesService.getArticles(filters);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  createArticle(@Body() article: CreateArticleDTO) {
    return this.articlesService.createArticle(article);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  @ApiOperation({ summary: 'Remove an item from the database' })
  @ApiResponse({
    status: 200,
    description: '',
    type: IndexArticleSwagger,
    isArray: true,
  })
  @ApiBearerAuth()
  removeArticleById(@Param('id') id: string): Promise<void> {
    return this.articlesService.removeArticleById(id);
  }
}
