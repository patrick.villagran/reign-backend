export class DateUtils {
  static getMonthByName(month: string): number {
    return new Date(Date.parse(month + ' 1, 2012')).getMonth() + 1;
  }
}
