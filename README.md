## Description

Back End Developer CHALLENGE

## Installation

* Clone repository
```
git clone https://gitlab.com/patrick.villagran/reign-backend.git
```
* Generate App

```
docker-compose up --build
```

## Getting started

* Get token 
```
curl -X POST http://localhost:3000/api/v1/auth/login -d '{"username": "patrick", "password": "reign"}' -H "Content-Type: application/json"
```

* Enter to the browser http://localhost:3000/api/articles

## Folders
```
├── auth  (user authentication)
├── config (System configurations)
├── modules ()
│   ├── articles (Hacker news articles entity)
│   └── tasks (automatic tasks)
├── users (users of the system)
└── utils (various utilities)
```
## NOTES

* Articles are the entity that was generated to store the news of hackers news.
* The Tasks service executes the data insertion automatically across cron.


