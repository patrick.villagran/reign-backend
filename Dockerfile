FROM node:16.13.0 as builder
LABEL stage=builder
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . .
RUN npm install && npm run build

FROM node:16.13.0
ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/package.json /usr/src/app/package-lock.json ./
COPY --from=builder /usr/src/app/dist ./dist/
RUN mkdir -p /usr/src/app/config
COPY --from=builder /usr/src/app/src/config ./src/config/
RUN npm ci --only=production
 
ENTRYPOINT [ "npm" ]
CMD [ "run", "start:prod" ]
EXPOSE 3000

